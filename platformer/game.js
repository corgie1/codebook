
const keys = 'LEFT,RIGHT,UP,DOWN,SPACE,W,A,S,D,R'
let pl, k,coins,badboys,enemySpawner

function preload() {
    this.load.image('background', './assets/background.png')
    this.load.image('flyguy', './assets/flyguy.png')
    this.load.image('stool', './assets/stool.png')
    this.load.image('coin', './assets/coin.png')
    this.load.image('badboy', './assets/badboy.png')

}

function create() {
    this.add.image(0, 0, 'background').setOrigin(0, 0)

    pl = this.physics.add.sprite(100, 100, 'flyguy')
    pl.setScale(4)
    pl.setCollideWorldBounds(true)
    pl.setBounce(0)

    let badboys = this.physics.add.group()
    const spawnEnemy = () => {

        let b = badboys.create(200,200,'badboy')
        b.setCollideWorldBounds(true)
        b.setVelocity(300)
        b.setBounce(1)
        b.setScale(.2)

    }

    spawnEnemy()
    enemySpawner = setInterval(spawnEnemy, 2300)

    let stools = this.physics.add.staticGroup()
    stools.create(500, 300, 'stool')
    stools.create(100, 100, 'stool')
    stools.create(100, 500, 'stool')

    let coins = this.physics.add.staticGroup()
    coins.create(100, 200, 'coin')
    coins.create(200, 200, 'coin')
    coins.create(400, 300, 'coin')

    this.physics.add.collider(pl, stools)
    this.physics.add.collider(pl, badboys)
    this.physics.add.collider(stools, badboys)


    k = this.input.keyboard.addKeys(keys)



}

function update() {

    if (k.R.isDown){
    clearInterval(enemySpawner)    
    this.scene.restart()

}
    if (k.LEFT.isDown) {
        pl.setVelocityX(-300)
    }

    else if (k.RIGHT.isDown) {
        pl.setVelocityX(+300)
    }
    if (pl.body.onFloor()) {
        if (k.UP.isDown) {
            pl.setVelocityY(-700)
        }
        pl.setDragX(2000)
    }

}

let config = {
    pixelArt: true,

    scene: { preload, update, create },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 1800 },
            debug: false,
        },
    },

}


new Phaser.Game(config)

