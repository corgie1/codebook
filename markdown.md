# Markdown Notes

*Corgis* are **really** ***cool*** and they can be fat but they are also skinny. They are the best kind of dog.

This is another `paragraph`.

## Sub heading

You can have up to six but normally use two.

![baby corgi](https://www.rover.com/blog/wp-content/uploads/2019/01/6342530545_45ec8696c8_b-960x540.jpg)

## Link Examples

<https://skilstak.io>

[skilstak](https://skilstak.io)

[![baby corgi](https://www.rover.com/blog/wp-content/uploads/2019/01/6342530545_45ec8696c8_b-960x540.jpg)](https://discordapp.com)

1. **NO** tail
1. Stubs for legs
1. Floppy ears

----

* Shed everywhere
* Dont like to walk
* Get a little chubby

Corgis  
are  
cool.

```
   Corgis
 are
                  cool.
```

```js
console.log('Corgis')
```

## Block Quotes

>This is a single paragraph in a block quote.

>this is two paragraphs.
>
>Here is the second paragraph.